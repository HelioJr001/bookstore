//
//  String+Extensions.swift
//  BookStore
//
//  Created by Helio Junior on 02/03/21.
//

import Foundation

extension String {
    
    func languageCorresponding() -> String {
        switch self {
        case "en": return "English"
        case "pt": return "Portuguese"
        default: return "?"
        }
    }
    
    
}
