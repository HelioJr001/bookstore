//
//  Double+Extensions.swift
//  BookStore
//
//  Created by Helio Junior on 02/03/21.
//

import Foundation

extension Double {
    
    func formatCurrency(code: String) -> String {
        let formatter = NumberFormatter()
        formatter.currencyCode = code
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        formatter.numberStyle = .currencyAccounting
        return formatter.string(from: NSNumber(value: self)) ?? "$\(self)"
    }
}
