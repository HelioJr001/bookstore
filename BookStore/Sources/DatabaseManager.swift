//
//  DatabaseManager.swift
//  BookStore
//
//  Created by Helio Junior on 01/03/21.
//

import Foundation
import CoreData
import UIKit

final class DatabaseManager {
    
    static var shared: DatabaseManager = DatabaseManager()
    
    //MARK: Properties
    private let keyObject = "DBBook"
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    // MARK: Methods
    func save(book: Book) {
        let db = NSEntityDescription.insertNewObject(forEntityName: keyObject, into: context)
        db.setValue(book.id, forKey: "id")
        db.setValue(book.title, forKey: "title")
        db.setValue(book.authors, forKey: "authors")
        db.setValue(book.thumbnail, forKey: "thumbnail")
        db.setValue(book.language, forKey: "language")
        db.setValue(book.description, forKey: "aDescription")
        db.setValue(book.availableForSale, forKey: "availableForSale")
        db.setValue(book.buyLink, forKey: "buyLink")
        db.setValue(book.price, forKey: "price")
        db.setValue(book.currencyCode, forKey: "currencyCode")
        
        do{
            try context.save()
        } catch let err {
            print("=> Error: \(err)")
        }
    }
    
    func delete(book: Book) {
        let bookList = self.getList()
        for bookObject in bookList {
            if let bookId = bookObject.value(forKey: "id") as? String, bookId == book.id {
                context.delete(bookObject)
            }
        }
        
        do{
            try context.save()
        } catch let err {
            print("=> Error: \(err)")
        }
    }
    
    func getList() -> [NSManagedObject]{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: keyObject)
        var bookList: [NSManagedObject] = []
        do {
            let address = try context.fetch(request)
            bookList = address as! [NSManagedObject]
        } catch{
            print("==> Error on request DBBook.")
        }
        return bookList
    }
}
