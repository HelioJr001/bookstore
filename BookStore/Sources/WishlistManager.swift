//
//  WishlistManager.swift
//  BookStore
//
//  Created by Helio Junior on 01/03/21.
//

import Foundation

final class WishlistManager {
    
    // MARK: Properties
    static var shared: WishlistManager = WishlistManager()
    var bookList: [Book] = []
    
    init() {
        loadBookList()
    }
    
    // MARK: Methods
    func hasFavorite(id: String) -> Bool {
        if let _ = bookList.filter({ $0.id == id }).first {
            return true
        } else {
            return false
        }
    }
    
    func toggleBookWith(book: Book) {
        if let _ = bookList.filter({ $0.id == book.id }).first {
            DatabaseManager.shared.delete(book: book)
        } else {
            DatabaseManager.shared.save(book: book)
        }
        loadBookList()
    }
    
    private func loadBookList() {
        let dbList = DatabaseManager.shared.getList()
        bookList = []
        
        for dbObject in dbList {
            let book = Book(from: dbObject)
            bookList.append(book)
        }
    }
}
