//
//  BookDetailViewController.swift
//  BookStore
//
//  Created by Helio Junior on 02/03/21.
//

import UIKit
import Kingfisher

class BookDetailViewController: UIViewController {

    // MARK: Properties
    private let book: Book
    
    // MARK: Outlets
    @IBOutlet weak var viewUp: UIView!
    @IBOutlet weak var viewDown: UIView!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var imageBook: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelAuthors: UILabel!
    @IBOutlet weak var labelLanguage: UILabel!
    @IBOutlet weak var labelAvailable: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var buttonBuy: UIButton!
    @IBOutlet weak var labelDescription: UILabel!
    
    // MARK: Initialization
    init(book: Book) {
        self.book = book
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    // MARK: Actions
    @IBAction func handlerButtonBuy(_ sender: Any) {
        guard let buyLink = book.buyLink, let link = URL(string: buyLink) else {return}
        if UIApplication.shared.canOpenURL(link) {
            UIApplication.shared.open(link, options: [:], completionHandler: nil)
        }
    }
    
    @objc func handlerButtonWishlist() {
        WishlistManager.shared.toggleBookWith(book: book)
        setupNavigationBar()
    }
    
    // MARK: Methods
    private func setupUI() {
        setupViews()
        setupLabels()
        setupButton()
        setupImage()
        setupNavigationBar()
    }
    
    private func setupViews() {
        let cornerRadius: CGFloat = 3
        viewUp.layer.cornerRadius = cornerRadius
        viewDown.layer.cornerRadius = cornerRadius
        viewImage.layer.borderWidth = 0.5
        viewImage.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    private func setupLabels() {
        labelTitle.text = book.title
        labelAuthors.text = book.authors
        labelLanguage.text = "Language: \(book.language?.languageCorresponding() ?? "")"
        labelDescription.text = book.description ?? "Sorry, no more information!"
        
        let available = book.availableForSale
        labelAvailable.text = available ? "Book available for purchase" : "Book unavailable"
        labelAvailable.textColor = available ? .darkGray : .red
        
        labelPrice.text = book.price?.formatCurrency(code: book.currencyCode ?? "BRL")
        labelPrice.isHidden = !book.availableForSale
    }
    
    private func setupButton() {
        buttonBuy.layer.cornerRadius = buttonBuy.layer.bounds.height / 2
        let available = book.availableForSale
        buttonBuy.isEnabled = available
        buttonBuy.backgroundColor = available ? UIColor.blue : UIColor.blue.withAlphaComponent(0.1)
    }
    
    private func setupImage() {
        if let imageUrl = book.thumbnail, let url = URL(string: imageUrl) {
            imageBook.kf.setImage(with: url)
        } else {
            imageBook.image = UIImage(named: "book-placeholder")
        }
    }
    
    private func setupNavigationBar() {
        let isFavorite = WishlistManager.shared.hasFavorite(id: book.id)
        let image = isFavorite ? UIImage(named: "star-full") : UIImage(named: "star-empty")
        let icon = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(handlerButtonWishlist))
        navigationItem.rightBarButtonItem = icon
    }
}
