//
//  BookResponseModel.swift
//  BookStore
//
//  Created by Helio Junior on 01/03/21.
//

import Foundation

struct BookResponseModel: Decodable {
    let kind: String
    let totalItems: Int
    let items: [Item]
    
    struct Item: Decodable {
        let kind: String
        let id: String
        let volumeInfo: VolumeInfo
        let saleInfo: SaleInfo
        
        struct VolumeInfo: Decodable {
            let title: String
            let authors: [String]?
            let publisher: String?
            let publishedDate: String?
            let description: String?
            let pageCount: Int?
            let averageRating: Int?
            let ratingsCount: Int?
            let contentVersion: String?
            let imageLinks: ImageLinks?
            let language: String?
            let previewLink: String?
            let infoLink: String?
        
            struct ImageLinks: Decodable {
                let thumbnail: String
            }
        }
        
        struct SaleInfo: Decodable {
            
            enum Saleability: String, Codable {
                case forSale = "FOR_SALE"
                case notForSale = "NOT_FOR_SALE"
                
                public init(from decoder: Decoder) throws {
                    self = try Saleability(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .notForSale
                }
            }
            
            let saleability: Saleability
            let buyLink: String?
            let listPrice: ListPrice?
            
            struct ListPrice: Decodable {
                let amount: Double
                let currencyCode: String
            }
        }
    }
    
    

}


/*
 "saleInfo": {
 "country": "BR",
 "saleability": "FOR_SALE",
 "isEbook": true,
 "listPrice": {
 "amount": 237.07,
 "currencyCode": "BRL"
 },
 "retailPrice": {
 "amount": 237.07,
 "currencyCode": "BRL"
 },
 "buyLink": "https://play.google.com/store/books/details?id=XzZ8y-Cz_BsC&rdid=book-XzZ8y-Cz_BsC&rdot=1&source=gbs_api",
 "offers": [
 {
 "finskyOfferType": 1,
 "listPrice": {
 "amountInMicros": 237070000,
 "currencyCode": "BRL"
 },
 "retailPrice": {
 "amountInMicros": 237070000,
 "currencyCode": "BRL"
 },
 "giftable": true
 }
 ]
 }
 */
