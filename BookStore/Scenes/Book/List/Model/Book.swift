//
//  Book.swift
//  BookStore
//
//  Created by Helio Junior on 01/03/21.
//

import Foundation
import CoreData

struct Book: Decodable {
    let id: String
    let title: String
    let authors: String?
    let thumbnail: String?
    let language: String?
    let description: String?
    let availableForSale: Bool
    let buyLink: String?
    let price: Double?
    let currencyCode: String?
    
    init(id: String, title: String, authors: String?, thumbnail: String?, language: String?, description: String?, availableForSale: Bool, linkBuy: String?, price: Double?, currencyCode: String?) {
        self.id = id
        self.title = title
        self.authors = authors
        self.thumbnail = thumbnail
        self.language = language
        self.description = description
        self.availableForSale = availableForSale
        self.buyLink = linkBuy
        self.price = price
        self.currencyCode = currencyCode
    }
    
    init(from dbObject: NSManagedObject) {
        id = dbObject.value(forKey: "id") as? String ?? ""
        title = dbObject.value(forKey: "title") as? String ?? ""
        authors = dbObject.value(forKey: "authors") as? String
        thumbnail = dbObject.value(forKey: "thumbnail") as? String
        language = dbObject.value(forKey: "language") as? String
        description = dbObject.value(forKey: "aDescription") as? String
        availableForSale = dbObject.value(forKey: "availableForSale") as? Bool ?? false
        buyLink = dbObject.value(forKey: "buyLink") as? String
        price = dbObject.value(forKey: "price") as? Double
        currencyCode = dbObject.value(forKey: "currencyCode") as? String
    }
    
    init(from item: BookResponseModel.Item) {
        id = item.id
        title = item.volumeInfo.title
        authors = item.volumeInfo.authors?.joined(separator: ", ")
        thumbnail = item.volumeInfo.imageLinks?.thumbnail
        language = item.volumeInfo.language
        description = item.volumeInfo.description
        self.availableForSale = item.saleInfo.saleability == .forSale
        self.buyLink = item.saleInfo.buyLink
        self.price = item.saleInfo.listPrice?.amount
        self.currencyCode = item.saleInfo.listPrice?.currencyCode
    }
}
