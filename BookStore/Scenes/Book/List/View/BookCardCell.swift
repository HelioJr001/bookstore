//
//  BookCardCell.swift
//  BookStore
//
//  Created by Helio Junior on 01/03/21.
//

import UIKit
import Kingfisher

class BookCardCell: UICollectionViewCell {
    
    static let identifier = "BookCardCell"
    
    // MARK: Properties
    private var book: Book!
    var didPressButtonWishlist: (() -> Void)?

    // MARK: Outlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageViewFavorite: UIImageView!
    
    // MARK: Overrides
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imageView.layer.cornerRadius = 3
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        imageView.image = UIImage(named: "book-placeholder")
        imageViewFavorite.image = UIImage(named: "star-empty")
    }
    
    // MARK: Actions
    @IBAction func handlerButtonFavorite(_ sender: Any) {
        didPressButtonWishlist?()
    }
    
    // MARK: Methods
    func setup(with book: Book) {
        self.book = book
        
        if let imageUrl = book.thumbnail, let url = URL(string: imageUrl) {
            imageView.kf.setImage(with: url)
        } else {
            imageView.image = UIImage(named: "book-placeholder")
        }
        
        imageViewFavorite.image = WishlistManager.shared.hasFavorite(id: book.id)
                                    ? UIImage(named: "star-full")
                                    : UIImage(named: "star-empty")
    }
}
