//
//  BookListService.swift
//  BookStore
//
//  Created by Helio Junior on 01/03/21.
//

import Foundation

final class BookListService {
    
    func getBooksiOSList(numberResults: Int, page: Int, completion: @escaping (Result<BookResponseModel, Error>) -> Void) {
        let link = "https://www.googleapis.com/books/v1/volumes?q=ios&maxResults=\(numberResults)&startIndex=\(page)"
        print("Request URL: \(link)")
        guard let url = URL(string: link) else {return}
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                completion(.failure(error))
            }
            
            do {
                guard let data = data else {return}
                let model = try JSONDecoder().decode(BookResponseModel.self, from: data)
                completion(.success(model))
            } catch let error {
                completion(.failure(error))
            }
        }.resume()
    }
}
