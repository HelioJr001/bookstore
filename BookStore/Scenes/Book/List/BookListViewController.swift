//
//  BookListViewController.swift
//  BookStore
//
//  Created by Helio Junior on 01/03/21.
//

import UIKit

final class BookListViewController: UIViewController {

    // MARK: Properties
    var viewModel: BookListViewModel
    
    // MARK: Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: Initialization
    init(viewModel: BookListViewModel = BookListViewModel()) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Book Store"
        setupUI()
        bindEvents()
        viewModel.fetchBooks()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        collectionView.reloadData()
    }
    
    // MARK: Actions
    @objc func handlerButtonWishlist() {
        viewModel.isFilterWishlist.toggle()
        setupNavigationBar()
    }
    
    // MARK: Methods
    func bindEvents() {
        viewModel.shouldUpdateLayout = { [weak self] in
            DispatchQueue.main.async {
                self?.collectionView.reloadData()
            }
        }
    }
    
    private func setupUI() {
        setupCollectionView()
        setupNavigationBar()
    }
    
    private func setupNavigationBar() {
        let image = viewModel.isFilterWishlist ? UIImage(named: "star-full") : UIImage(named: "star-empty")
        let icon = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(handlerButtonWishlist))
        navigationItem.rightBarButtonItem = icon
    }
    
    private func setupCollectionView() {
        collectionView.register(UINib(nibName: BookCardCell.identifier, bundle: nil), forCellWithReuseIdentifier: BookCardCell.identifier)
        let widthScreen = UIScreen.main.bounds.width
        let margins: CGFloat = 50
        let widthCard = (widthScreen - margins) / 2
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: widthCard, height: 1.5 * widthCard)
        collectionView.setCollectionViewLayout(layout, animated: true)
        collectionView.backgroundColor = .clear
        collectionView.dataSource = self
        collectionView.delegate = self
    }
}

// MARK: Extensions
extension BookListViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.getListCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let book = viewModel.getList()[indexPath.item]
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BookCardCell.identifier, for: indexPath) as? BookCardCell {
            cell.setup(with: book)
            cell.didPressButtonWishlist = { [weak self] in
                WishlistManager.shared.toggleBookWith(book: book)
                self?.collectionView.reloadData()
            }
            return cell
        }
        
        return UICollectionViewCell()
    }
}

extension BookListViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if indexPath.item > viewModel.getList().count - 4 && !viewModel.lockRequest {
            viewModel.paginationList()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let book = viewModel.getList()[indexPath.item]
        
        let viewController = BookDetailViewController(book: book)
        navigationController?.pushViewController(viewController, animated: true)
    }
}
