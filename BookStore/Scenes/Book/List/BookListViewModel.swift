//
//  BookListViewModel.swift
//  BookStore
//
//  Created by Helio Junior on 01/03/21.
//

import Foundation

final class BookListViewModel {
    
    // MARK: Properties
    private let service: BookListService
    var lockRequest = false
    private var totalItems = 0
    private let numberResults = 20
    private var pageCurrent = 0
    private var list: [Book] = []
    var shouldUpdateLayout: (() -> Void)?
    var isFilterWishlist = false {
        didSet{
            shouldUpdateLayout?()
        }
    }
    
    init(service: BookListService = BookListService()) {
        self.service = service
    }
    
    // MARK: Methods
    func fetchBooks() {
        service.getBooksiOSList(numberResults: numberResults, page: pageCurrent) { [weak self] response in
            self?.pageCurrent += 1
            
            switch response {
            case .success(let model):
                let bookList = model.items.compactMap({ Book(from: $0) })
                self?.list = bookList
                self?.totalItems = model.totalItems
                self?.shouldUpdateLayout?()
            case .failure(let error):
                print("==> Error: \(error.localizedDescription)")
            }
        }
    }
    
    func paginationList() {
        guard !isFilterWishlist else {return}
        lockRequest = true
        let numberResults = calculateNumberItemsRemaining()
        
        service.getBooksiOSList(numberResults: numberResults, page: pageCurrent) { [weak self] response in
            self?.pageCurrent += 1
            self?.lockRequest = false
            
            switch response {
            case .success(let model):
                let bookList = model.items.compactMap({ Book(from: $0) })
                self?.list.append(contentsOf: bookList)
                self?.shouldUpdateLayout?()
            case .failure(let error):
                print("==> Error: \(error.localizedDescription)")
            }
        }
    }
    
    private func calculateNumberItemsRemaining() -> Int {
        let numberItemsRemaining = totalItems - list.count
        
        if numberItemsRemaining > numberResults {
            return numberResults
        } else {
            return numberItemsRemaining
        }
    }
    
    func getListCount() -> Int {
        if isFilterWishlist {
            return WishlistManager.shared.bookList.count
        } else {
            return list.count
        }
    }
    
    func getList() -> [Book] {
        if isFilterWishlist {
            return WishlistManager.shared.bookList
        } else {
            return list
        }
    }
}
